NAME=stammtisch
CXX := g++
CXXFLAGS := -c -g -Wall
LDFLAGS := -g -lboost_date_time `pkg-config --libs libical`

SRCDIR := src
OBJDIR := obj
BINDIR := bin

cppfiles := $(wildcard $(SRCDIR)/*.cpp)
objects := $(subst $(SRCDIR)/, $(OBJDIR)/, $(cppfiles:.cpp=.o))
deps := $(objetcts:.o=.d)

.PHONEY: all clean

all: $(BINDIR)/$(NAME)
-include $(deps)

$(BINDIR)/$(NAME): $(objects)
	$(CXX) $(LDFLAGS) -o $@ $^

$(OBJDIR)/%.d: $(SRCDIR)/%.cpp
	$(CXX) -MM -MT "$@ $(patsubst $.d,%.o,$@)" -MF $@ $<

$(OBJDIR)/%.o: $(SRCDIR)/%.cpp
	$(CXX) $(CXXFLAGS) $< -o $@

clean:
	rm -f $(objects) $(deps)

format:
	clang-format -i $(SRCDIR)/*
