#ifndef STAMMTISCH_HPP_
#define STAMMTISCH_HPP_

#define PRODID "-//Stammisch Software//devLUG//DE"
#define UID "devlug.de"
#define ORGANIZER "mailto:kontakt@devlug.de"
#define SUMMARY "Stammtisch devLUG"
#define DESCRIPTION "Stammtisch der Virtuellen Linux User Group im IRC Freenode"
#define LOCATION "IRC Freenode #devlug"
#define TZID "Europe/Berlin"

#include <boost/date_time/date_defs.hpp>
#include <boost/date_time/gregorian/gregorian.hpp>
#include <list>

namespace devlug
{
namespace stammtisch
{

class StammtischCal
{
  public:
    StammtischCal();
    std::list<boost::gregorian::date> getAppointments();

  protected:
  private:
    std::list<boost::gregorian::date> appointmentList;
};

class Output
{
  protected:
    Output(StammtischCal *);
    StammtischCal *cal;
};

class OutputConsole : public Output
{
  public:
    OutputConsole(StammtischCal *);
    void print();
};

class OutputICal : public Output
{
  public:
    OutputICal(StammtischCal *);
    void print();
};
}
}

#endif
